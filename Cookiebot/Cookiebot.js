var cookieBannerSliderPos = 0;

function showCookieBanner() {
    var cookiebanner = document.getElementById("cookiebanner");
    var dialogHeight = parseInt(cookiebanner.offsetHeight);
    cookiebanner.style.bottom = (cookieBannerSliderPos - dialogHeight) + "px";
    cookieBannerSliderPos += 4;
    if (cookieBannerSliderPos < dialogHeight) {
        setTimeout(function () {
            showCookieBanner();
        }, 1);
    } else {
        cookieBannerSliderPos = 0;
        cookiebanner.style.bottom = "0px";
    }
}

function hideCookieBanner() {
    var cookiebanner = document.getElementById("cookiebanner");
    cookiebanner.style.display = "none";
}
$(document).on("click", ".c-change-btn .c-button", function(){
    var x = document.getElementById("js-cookie-selection");
    if (x.style.display === "none") {
        x.style.display = "block";
        document.getElementById("js-cookie-buttons").style.display = "none";
        document.getElementById("js-cookie-buttons-customize").style.display = "flex";
    } 
});
