/*
 * Copy widget
 * Version 8.0
 * Added discount calculator
 * Added if/else check for old price element
 * Added an AJAX trigger that re-executes the script
 * Made the script a stand-alone function
 * Added a delay in the AJAX trigger so that the data is always available
 * Fixed the 0 EUR discount display error
 * Added a delay on the normal load so that the data is available
 * Finetuned the AKAX delay
 * Fixed the popup problem
 * Added a check that only adds the sentence if it doesnt have any
 * Added a fix for the AJAX bill
 */
/* CUSTOM CODE */
/*vwo_*/$(window).load(function () {
    setTimeout(function () {
        replaceTotal();
    }, 300);
    $(document).ajaxStop(function () {
        setTimeout(function () {
            replaceTotal();
        }, 300);
    });
});
function replaceTotal() {
    //Check om te kijken of er een oude prijs bestaat
    if (document.getElementsByClassName("price__old").length > 0) {
        //korting berekening
            //Oude en nieuwe prijs ophalen uit de code
        var oud = document.querySelector(".price__list-total .price__old").innerHTML;
        var nieuw = document.querySelector(".price__list-total .price").innerHTML;
        //Formatteren van de getallen
        nieuw = nieuw.slice(0, -2);
        oud = oud.slice(0, -2);
        nieuw = nieuw.replace(/\./g, "");
        oud = oud.replace(/\./g, "");
        var nieuwINT = parseInt(nieuw, 10);
        var oudINT = parseInt(oud, 10);
        //Korting berekenen
        var kortingINT = oudINT - nieuwINT;
        //prijs copy widgets voor acco's met korting
        var quotes = new Array("U heeft de beste prijs", "Dit is de laagste prijs", "Nu € " + kortingINT + ",- korting", "U bespaart € " + kortingINT + ",-"),
        randno = quotes[Math.floor(Math.random() * quotes.length)];
        //Check om te kijken of er al een prijs widget bestaat in de bill
        if ($(".summary__price td:first:contains('prijs'),.summary__price td:first:contains('korting'),.summary__price td:first:contains('bespaart')").length >= 1) {
            //do nothing
        } else {
            $(".summary__price td")[0].innerHTML = (randno);
        }
        //Check of de price popup bill al bestaat in de HTML, als die nog niet geopend is dan 'bestaat' hij nog niet
        var popup = $(".price__popup__inner");
        if (popup != null) {
            //Een loop die zoekt naar het juiste element in de price popup bill en daar de prijs widget toevoegd
            $(".price__popup__inner").each(function () {
                if ($(this).find(".price__list-total").length >= 1) {
                    var kek = $(this).find(".price__list-total td:first-child");
                    $(kek).text(randno);
                } else {
                }
            });
        }
    } else {
        //prijs copy widget voor een normale prijs zonder korting
        var quotes = new Array("U heeft de beste prijs", "Dit is de laagste prijs"),
                randno = quotes[Math.floor(Math.random() * quotes.length)];
        //Check om te kijken of er al een prijs widget bestaat in de bill
        if ($(".summary__price td:first:contains('prijs')").length >= 1) {
            //do nothing
        } else {
            $(".summary__price td")[0].innerHTML = (randno);
        }
        //Check of de price popup bill al bestaat in de HTML, als die nog niet geopend is dan 'bestaat' hij nog niet
        var popup = $(".price__popup__inner td")[4];
        if (popup != null) {
            //Een loop die zoekt naar het juiste element in de price popup bill en daar de prijs widget toevoegd
            $(".price__popup__inner").each(function () {
                if ($(this).find(".price__list-total").length >= 1) {
                    var kek = $(this).find(".price__list-total td:first-child");
                    $(kek).text(randno);
                } else {
                }
            });
        }
    }
//De css verbergt deze element tijdelijk zodat het niet flikkert als totaal wordt vervangen door een prijs widget, de code hieronder maakt ze weer zichtbaar
    $(".price__list-total td").first().css("visibility", "visible");
    $(".summary__price td").first().css("visibility", "visible");
}