//This script changes all button texts of a parc card/Object card on the PDP/ADP of the landal site
// select the target node
const shadowDomWrapper = document.querySelector('result-list-component');
const elementInsideShadowDom = shadowDomWrapper.shadowRoot.querySelector('.result-list');

// create an observer instance
var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        if(mutation.type === "childList" && mutation.target == shadowDomWrapper.shadowRoot.querySelector('div.card-grid-layout')){
            changeButtons();
        }
    });    
});

// configuration of the observer:
var config = { attributes: true, childList: true, characterData: true, subtree: true }

// pass in the target node, as well as the observer options
observer.observe(elementInsideShadowDom, config);


function changeButtons(){
    const buttonNodeList = document.querySelector("result-list-component").shadowRoot.querySelectorAll(".result-list .c-button.c-button--action.c-button--full"); 

    buttonNodeList.forEach((buttonNode) => {
        buttonNode.textContent = "Test";
    });
}

changeButtons();